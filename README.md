## Работа с git
    git init
    git remote add origin git@gitlab.com:apexlife.ru/REPOSITORYNAME
    git clone git@gitlab.com:apexlife.ru/frontend-blank.git

переносим содержимое папки ***frontend-blank*** в общую папку проекта и удаляем саму папку ***frontend-blank***

    git add .
    git commit -m "add README"
    git push -u origin master
    
<hr>
    
## Gulp install
Для работы gulp требуется установить следующие пакеты: <br>

`npm install gulp gulp-sourcemaps gulp-rename gulp-notify gulp-sass gulp-autoprefixer gulp-csscomb gulp-concat gulp-minify gulp-uglify gulp-clean-css gulp-uncss gulp-htmlmin gulp-util gulp-ftp gulp.spritesmith gulp-svg-sprites --save-dev`

<hr>

## Запуск комиляций
`gulp watch`

<hr>

## Указать пути к серверу
`ctrl + g` : `64`

    host: 'ftp.apexlife.ru',
    user: 'username',
    pass: 'password'

<hr>

## Загрузить на сервер
`gulp ftp`
