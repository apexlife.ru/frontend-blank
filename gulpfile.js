var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    csscomb = require('gulp-csscomb'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    uncss = require('gulp-uncss'),
    htmlmin = require('gulp-htmlmin'),
    gutil = require('gulp-util'),
    spritesmith = require('gulp.spritesmith'),
    ftp = require('gulp-ftp');

//GULP SCSS CONCAT CSSCOMB CLEANCSS AUTOPREFIXER
gulp.task('css', function () {
    return gulp.src('assets/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('cssmin/bundle.css'))
        .pipe(csscomb("cssmin/bundle.css"))
        .pipe(cleanCSS("cssmin/bundle.css"))
        .pipe(rename("style.min.css"))
        .pipe(uncss({
            html: ['*.html']
        }))
        .pipe(gulp.dest('assets/css/'))
        .pipe(notify('Done!'))
});
//GULP JAVASCRIPT CONCAT + MINIFY + UGLIFY
gulp.task('js', function() {
    return gulp.src(['assets/js/libs/jquery.js', 'assets/js/libs/webfont.js', 'assets/js/libs/bootstrap.js', 'assets/js/script.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(rename('production.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'));
});
//GULP HTML Minify
gulp.task('htmlMinify', function() {
    return gulp.src('*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('htmlmin'));
});
//GULP FTP
gulp.task('ftp', function () {
    return gulp.src([
        '**/*',
        '*',
        '!node_modules/**',
        '!bower_components/**',
        '!bundle.css/**',
        '!cssmin/**',
        '!htmlmin/**'
    ])
        .pipe(ftp({
            host: 'ftp.apexlife.ru',
            user: 'username',
            pass: 'password'
        }))

        .pipe(gutil.noop());
});
//GULP SPRITE
gulp.task('sprites', function () {
    var spriteData = gulp.src('assets/img/icons/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
        padding: 20
    }));
    return spriteData.pipe(gulp.dest('assets/css/'));
});
//GULP SVG
gulp.task('svg', function () {
    return gulp.src('assets/img/svg/**/*.svg')
        .pipe(svgSprite())
        .pipe(gulp.dest("assets/svg/"));
});
//GULP WATCH
gulp.task('watch', function () {
    gulp.watch('assets/sass/**/*.scss', ['css']);
    gulp.watch('assets/js/script.js', ['js']);
    gulp.watch('*.html', ['htmlMinify']);
    gulp.watch('assets/img/icons/*.png', ['sprites']);
});